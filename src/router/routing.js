import { createBrowserRouter } from "react-router-dom";
import Login from "../components/pages/login/Login";
import Home from "../components/pages/home/Home";
import { PATH, PORTALS_NAMES } from "../utils/constants";
import { Navigate } from "react-router-dom";
import Child from "../components/pages/child/Child";

console.log('Object.values(PATH)', Object.values(PATH))
export const routing = createBrowserRouter([
    {
        path: "/login",
        element: <Login />
    },
    {
        path: "/",
        element: <Home />,
        children:
            [...Object.values(PATH).filter(x => x.private).map(portal => ({
                path: portal.path,
                children:
                    Object.values(portal.children).length !== 0 ?
                        [
                            ...Object.values(portal.children)?.map(child => ({
                                path: child.path,
                                element: child.element
                            }),
                                {
                                    path: portal.path,
                                    element: <Navigate to={Object.values(portal.children)[0].path} />
                                },)] : null
            })),
                // {
                //     path: "/",
                //     element: <Navigate to={PATH[PORTALS_NAMES["ABOUT"]].children.ABOUT.path} />
                // },
                // {
                //     path: "*",
                //     element: <Navigate to={PATH[PORTALS_NAMES["ABOUT"]].children.ABOUT.path} />
                // },
            ]
    }

])  