import React from 'react'
import styles from "./LeftSideBar.module.scss"
import { sideBarNavigation } from '../../../utils/SideBarNavigation'
import { PORTALS } from '../../../utils/constants'
import { NavLink, useLocation } from 'react-router-dom'
import { useState, useMemo } from 'react'

function LeftSideBar() {
    const location = useLocation()
    const sidebarObj = useMemo(() => {
        const temp = Object.keys(PORTALS)
        return temp?.length && temp.reduce((acc, item) => {
            acc[item] = false
            return acc
        }, {})
    }, [])
    console.log('sideBarNavigation', sideBarNavigation)
    console.log('sidebarObj', sidebarObj)
    const [sidebarItemsOpen, setSidebarItemsOpen] = useState(sidebarObj);
    return (
        <div className={styles.left}>
            {Object.entries(PORTALS).length &&
                Object.entries(PORTALS).map(([key, value]) => {
                    return (
                        <div>
                            <NavLink
                                style={{ textDecoration: "none" }}
                                onClick={() => setSidebarItemsOpen({ ...sidebarItemsOpen, [key]: !sidebarItemsOpen?.[key] })}
                                to={value}
                            >
                                <p className={styles.nav}>{key}</p>
                            </NavLink>
                            {sidebarItemsOpen?.[key] && sideBarNavigation?.[key]?.map((data) => (
                                <NavLink
                                    style={{ textDecoration: "none" }}
                                    key={data.id}
                                    to={data.path}
                                    state={{ previousPath: location.pathname }}
                                >
                                    <p className={styles.nav}>{data.pageName}</p>
                                </NavLink>
                            ))}
                        </div>
                    )
                })
            }

        </div>
    )
}

export default LeftSideBar