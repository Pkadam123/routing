import React from 'react'
import DataTable from "react-data-table-component"

function Table({ columns, data }) {
    return (
        <>
            <DataTable
                pagination
                columns={columns}
                data={data}
            />
        </>
    )
}

export default Table