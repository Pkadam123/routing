import React, { useEffect, useState } from 'react'
import Table from '../../table/Table';
import axios from "axios"

function Name() {
    const [data, Setdata] = useState([])
    const columns = [
        {
            name: 'name',
            selector: row => row.name,
        },
        {
            name: 'email',
            selector: row => row.email,
        },
        {
            name: 'phone',
            selector: row => row.phone,
        }
    ];
    // axios.interceptors.request.use(function (config) {
    //     // Do something before request is sent
    //     document.getElementById("overlay").style.display = "block"
    //     console.log('config', config)
    //     return config;
    // }, function (error) {
    //     // Do something with request error
    //     return Promise.reject(error);
    // });

    // Add a response interceptor
    // axios.interceptors.response.use(function (response) {
    //     console.log('response', response)
    //     document.getElementById("overlay").style.display = "none"

    //     // Any status code that lie within the range of 2xx cause this function to trigger
    //     // Do something with response data
    //     return response;
    // }, function (error) {
    //     // Any status codes that falls outside the range of 2xx cause this function to trigger
    //     // Do something with response error
    //     return Promise.reject(error);
    // });
    useEffect(() => {
        let call = async () => {
            let data = await axios.get("https://64ccec742eafdcdc851a7719.mockapi.io/users")
            Setdata(data.data)
        }
        call()
    }, [])
    return (
        <div>
            <Table
                columns={columns}
                data={data}
            />
        </div>
    )
}

export default Name