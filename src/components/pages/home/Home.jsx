import React from 'react'
import LeftSideBar from '../../layouts/leftsidebar/LeftSideBar'
import { Outlet } from 'react-router-dom'
import styles from "./Home.module.scss"

function Home() {
    return (
        <div className={styles.container}>
            <LeftSideBar />
            <div className={styles.right}>
                <Outlet />
            </div>





        </div>
    )
}

export default Home