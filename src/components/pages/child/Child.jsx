import React from 'react'
import styles from "./Child.module.scss"

function Child() {
    return (
        <>
            <div className={styles.main}>
                <div className={styles.box}>1</div>
                <div className={styles.box}>2</div>

            </div>
            <div className={styles.main}>
                <div className={styles.box}>1</div>
                <div className={styles.box}>2</div>

            </div>

        </>

    )
}

export default Child