import React, { useState } from 'react'
import { Input } from '../../shared/inputfield/Input'
import { Button } from '../../shared/button/Button'
import styles from './Login.module.scss'
// import axios from "axios"

function Login() {
    const [value, setValue] = useState({})
    const [error, setError] = useState({
        email: "",
        password: ""
    })
    function handleSubmit(e) {
        e.preventDefault()
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!value.email || !emailRegex.test(value.email)) {
            setError({
                ...error,
                email: "invalid email"
            });
            return;
        }
        if (value.password.length < 3) {
            setError({
                ...error,
                password: "invalid password"
            });
            return;
        }
        setError({
            name: "",
            email: ""
        })
        // let post = async () => {
        //     try {
        //         const data = await axios.post("http://localhost:8080/api/login", value)
        //         console.log(data.data)
        //     } catch (err) {
        //         console.log(err)

        //     }
        // }
        // post()
        console.log(value)
    }
    function handleChange(e) {
        setValue({
            ...value,
            [e.target.name]: e.target.value
        })
    }
    return (
        <div>
            <form onSubmit={handleSubmit} className={styles.form}>
                <Input onChange={handleChange}

                    name="email"
                    label="Email"
                    required={true}
                    value={value?.email} />
                {error?.email && <p style={{ color: "red" }}>{error?.email}</p>}
                <Input
                    onChange={handleChange}
                    type="password"
                    name="password"
                    label="Password"
                    value={value?.password} />
                {error?.password && <p style={{ color: "red" }}>{error?.password}</p>}
                <div style={{ textAlign: "center" }}>
                    <Button
                        label="Login"
                        type="submit" />
                </div>
            </form>
        </div>
    )
}

export default Login