import React from 'react'
import styles from "./Input.module.scss"

export const Input = (props) => {
    return (
        <>
            <label>{props?.label} {props?.required ? <span style={{ color: "red" }}>*</span> : ""} : </label>
            <input
                className={styles.input}
                onChange={(e) => props?.onChange(e)} r
                required={props?.required}
                value={props?.value}
                name={props?.name}
                type={props?.type}
            />

        </>
    )
}
