import Name from "../components/pages/name/Name"
import Login from "../components/pages/login/Login"
import Child from "../components/pages/child/Child"
export const PORTALS_NAMES = {
    "ABOUT": "ABOUT",
    "TASK": "TASK",
    "USER": "USER",
    "SINGLE": "SINGLE"
}
export const PORTALS = {
    [PORTALS_NAMES["ABOUT"]]: "/about",
    [PORTALS_NAMES["TASK"]]: "/task",
    [PORTALS_NAMES["USER"]]: "/user",
    [PORTALS_NAMES["SINGLE"]]: "/single"
}

export const PATH = {
    DEFAULT: {
        path: "/",
        pageName: "Home"
    },
    LOGIN: {
        path: "/login",
        pageName: "Login"
    },
    [PORTALS_NAMES["ABOUT"]]: {
        path: PORTALS[[PORTALS_NAMES["ABOUT"]]],
        private: true,
        children: {
            NAME: {
                path: PORTALS[[PORTALS_NAMES["ABOUT"]]] + "/name",
                pageName: "name",
                element: <Name />,
                showGenerateBtn: true,
                sidebar: {
                    // name: "stocks"
                },
            },
        }
    },
    [PORTALS_NAMES["TASK"]]: {
        path: PORTALS[[PORTALS_NAMES["TASK"]]],
        private: true,
        children: {
            CHILD: {
                path: PORTALS[[PORTALS_NAMES["TASK"]]] + "/child",
                pageName: "child",
                element: <Child />,
                showGenerateBtn: true,
                sidebar: {
                    // name: "stocks"
                },
            },
        }
    },
    [PORTALS_NAMES["USER"]]: {
        path: PORTALS[[PORTALS_NAMES["USER"]]],
        private: true,
        children: {
            USER1: {
                path: PORTALS[[PORTALS_NAMES["USER"]]] + "/user1",
                pageName: "user",
                element: <Child />,
                showGenerateBtn: true,
                sidebar: {
                    // name: "stocks"
                },
            },
        }
    },
    [PORTALS_NAMES["SINGLE"]]: {
        path: PORTALS[[PORTALS_NAMES["SINGLE"]]],
        private: true,
        children: {

        }

    }

}