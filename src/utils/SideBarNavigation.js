import { PATH, PORTALS_NAMES } from "./constants";

export const sideBarNavigation = Object.values(PORTALS_NAMES).reduce((acc, portalName) => {
    acc[portalName] = Object.values(PATH[portalName].children)
        .filter(x => x.sidebar)
        .map((pageData, index) => ({
            id: index + 1,
            path: pageData.path,
            pageName: pageData.sidebar.name || pageData.pageName
        }))
    return acc
}, {})
console.log('sideBarNavigation', sideBarNavigation)
